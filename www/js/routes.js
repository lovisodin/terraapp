angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('terraforma', {
    url: '/index',
    templateUrl: 'templates/terraforma.html',
    controller: 'terraformaCtrl'
  })

  .state('tracking', {
    url: '/tracking',
    templateUrl: 'templates/tracking.html',
    controller: 'trackingCtrl'
  })

  .state('plant', {
    url: '/plant',
    templateUrl: 'templates/plant.html',
    controller: 'plantCtrl'
  })

  .state('succeed', {
    url: '/succeed',
    templateUrl: 'templates/succeed.html',
    controller: 'succeedCtrl'
  })

  .state('share', {
    url: '/share',
    templateUrl: 'templates/share.html',
    controller: 'shareCtrl'
  })

$urlRouterProvider.otherwise('/index')

  

});